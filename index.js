
const filmLink = "https://ajax.test-danit.com/api/swapi/films";
const filmRequest = fetch(filmLink).then(response => response.json());

const div = document.createElement('div');
document.body.prepend(div);

function showFilms(filmlist){
    filmlist.forEach((film) => {
        const filmInfo = document.createElement('ul');
        const charactersList = document.createElement('ul');  
        // charactersList.classList.add('characters-list') 
        film.characters.forEach(link => {
            fetch(link).then(response => response.json())
            .then(character => {
                const li = document.createElement('li');
                li.innerText = character.name;
                charactersList.append(li)
            })
        })
        filmInfo.innerHTML =`<li>Episode : ${film.episodeId}</li> <li>${film.name}</li> <li>${film.openingCrawl}</li>`;
        div.append(filmInfo, charactersList);
    })
}

filmRequest.then((films) => {
    showFilms(films)
})